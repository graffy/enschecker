tool to check declared teaching hours from csv file extracted from OSE (Organisation des Services d'Enseignement)

Usage:

```sh
python3 ./main.py
```

Generates the `df3.csv` from `sample.csv`.
