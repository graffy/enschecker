import pandas


def main():
    # dtype={'Période': str}
    df = pandas.read_csv('sample.csv', sep=';', na_filter=False)
    # print(df.columns)
    print(df.dtypes)
    periode = df['Période']
    print(type(periode))  # Series
    print(periode.dtype)
    # print(df.to_string())

    # df2 = df.groupby('Enseignement ou fonction référentielle').sum()
    df2 = df.groupby(['Enseignement ou fonction référentielle', 'Période', 'Formation ou établissement'])['CM', 'TD', 'TP'].sum()
    print(df2.shape)
    print(df2.to_string())

    df3 = df2.groupby(['Période', 'Formation ou établissement'])['CM', 'TD', 'TP'].sum()
    print(df3.shape)
    print(df3.to_string())
    df3.to_csv('df3.csv')
    # CM;TD;TP

    # a Période : S1
    # b Formation ou établissement : L1 Portail Mathématiques et  Applications S1 S2
    # c Enseignement ou fonction référentielle (ue) : PM1-Physique et Mécanique 1
    # a1 b1 c1 x1 y1
    # a1 b1 c1 x5 y5
    # a1 b1 c2 x2 y2
    # a1 b1 c1 x3 y3
    # a1 b2 c2 x4 y4

    # tab1
    # a1 b1 c1 : x1 + x3 + x5,  y1 + y3 + y5,    x1 + x3 + x5 + y1 + y3 + y5
    # a1 b2 c2 : x2 + x4,       y2 + y4,         x2 + x4 + y2 + y4

    # tab c1

    # tab c2


main()
